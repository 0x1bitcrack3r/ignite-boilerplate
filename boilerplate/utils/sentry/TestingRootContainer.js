/**
 * @flow
 * @author: 0x1bitcrack3r
 */

import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import { observer } from 'mobx-react';

import { Sentry } from 'react-native-sentry';

// Others
import NavigationRouter from '../../navigation/NavigationRouter';

// Styles
import styles from './styles';

Sentry.config(
  'REDACTED',
  {
    autoBreadcrumbs: {
      console: !__DEV__ // to get the file name and line numbers in dev mode debugging.
    }
  }
).install();

@observer
class RootContainer extends Component {
  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle="light-content" />
        <NavigationRouter />
      </View>
    );
  }
}

export default RootContainer;
