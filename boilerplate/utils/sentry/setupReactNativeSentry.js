const setupReactNativeSentry = async function(system, filesystem, print) {
  const getPackageJson = filesystem => {
    try {
      const jsonContent = filesystem.read(
        `${process.cwd()}/package.json`,
        'utf8'
      );
      return JSON.parse(jsonContent);
    } catch (error) {
      print.info(error);
    }
    return false;
  };

  const writePackageJson = (filesystem, packageJson) => {
    try {
      const content = `${JSON.stringify(packageJson, null, 2)}\n`;
      filesystem.write(`${process.cwd()}/package.json`, content);
    } catch (error) {
      print.info(error);
    }
  };

  await system.spawn(`yarn add react-native-sentry@0.35.3 --ignore-scripts`, {
    stdio: 'inherit'
  });
  filesystem.copy(
    `${__dirname}/SentryUtils.js`,
    `${process.cwd()}/App/Utils/SentryUtils.js`,
    {
      overwrite: true
    }
  );
  filesystem.copy(
    `${__dirname}/sentry/sentry.properties.alpha`,
    `${process.cwd()}/sentry/sentry.properties.alpha`,
    {
      overwrite: true
    }
  );
  filesystem.copy(
    `${__dirname}/sentry/sentry.properties.beta`,
    `${process.cwd()}/sentry/sentry.properties.beta`,
    {
      overwrite: true
    }
  );
  filesystem.copy(
    `${__dirname}/sentry/sentry.properties.gamma`,
    `${process.cwd()}/sentry/sentry.properties.gamma`,
    {
      overwrite: true
    }
  );
  filesystem.copy(
    `${__dirname}/sentry/sentry.properties.prod`,
    `${process.cwd()}/sentry/sentry.properties.prod`,
    {
      overwrite: true
    }
  );
  filesystem.copy(
    `${__dirname}/sentry/sentry.properties.dev`,
    `${process.cwd()}/sentry/sentry.properties.dev`,
    {
      overwrite: true
    }
  );
  // adding scripts in package.json
  const packageJson = getPackageJson(filesystem);
  if (packageJson) {
    packageJson.scripts = packageJson.scripts || {};
    packageJson.scripts['android:build'] =
      'cp sentry/sentry.properties.dev android/sentry.properties && cp sentry/sentry.properties.dev ios/sentry.properties && ' +
      packageJson.scripts['android:build'];

    packageJson.scripts['android:abuild'] =
      'cp sentry/sentry.properties.alpha android/sentry.properties && cp sentry/sentry.properties.alpha ios/sentry.properties && ' +
      packageJson.scripts['android:abuild'];

    packageJson.scripts['android:bbuild'] =
      'cp sentry/sentry.properties.beta android/sentry.properties && cp sentry/sentry.properties.beta ios/sentry.properties && ' +
      packageJson.scripts['android:bbuild'];

    packageJson.scripts['android:gbuild'] =
      'cp sentry/sentry.properties.gamma android/sentry.properties && cp sentry/sentry.properties.gamma ios/sentry.properties && ' +
      packageJson.scripts['android:gbuild'];

    packageJson.scripts['android:pbuild'] =
      'cp sentry/sentry.properties.prod android/sentry.properties && cp sentry/sentry.properties.prod ios/sentry.properties && ' +
      packageJson.scripts['android:pbuild'];
    writePackageJson(filesystem, packageJson);
  }

  // adding jest mock environments
  filesystem.copy(
    `${__dirname}/react-native-sentry.js`,
    `${process.cwd()}/__mocks__/react-native-sentry.js`,
    {
      overwrite: true
    }
  );
  // adding test case
  filesystem.copy(
    `${__dirname}/sentry.test.js`,
    `${process.cwd()}/__tests__/sentry.test.js`,
    {
      overwrite: true
    }
  );
};

module.exports = setupReactNativeSentry;
