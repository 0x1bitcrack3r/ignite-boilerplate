/**
 * @flow
 * @author: 0x1bitcrack3r
 * */

export const Sentry = {
  config: jest.fn(() => Sentry),
  install: jest.fn(),
  captureMessage: jest.fn(),
  captureException: jest.fn(),
  captureBreadcrumb: jest.fn()
}
