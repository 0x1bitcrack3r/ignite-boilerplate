/**
 * @flow
 * @author: 0x1bitcrack3r
 */

import { Sentry } from 'react-native-sentry'
import { displayApiError } from '../CommonMethods.js'

export const sendSentryError = (url, requestObject, error) => {
  try {
    const customErrorObject = displayApiError(error)
    const actualErrorObject = JSON.parse(error.message).data
    // sentry is stripping the sensitive data - https://docs.sentry.io/learn/sensitive-data/
    // baseURL: JSON.stringify(api.getBaseURL()),
    const extraData = {
      url,
      requestObject,
      customErrorObject,
      actualErrorObject
    }
    if (__DEV__) console.log('sentry extraData', extraData)
    const messageName = `Api Failure ${url}`
    Sentry.captureMessage(messageName, {
      stacktrace: true,
      extra: extraData,
      level: SentrySeverity.Error
    })
  } catch (e) {
    if (__DEV__) console.log('errors in sending log to sentry', e)
  }
}
