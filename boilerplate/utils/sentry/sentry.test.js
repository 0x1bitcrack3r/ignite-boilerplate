/**
 * @flow
 * @author: 0x1bitcrack3r
 **/

import { Sentry } from 'react-native-sentry'

test('mocking sentry', () => {
  Sentry.config().install()
  Sentry.captureMessage('messageName', {
    stacktrace: true
  })
  Sentry.captureException({
    stacktrace: true
  })
})
