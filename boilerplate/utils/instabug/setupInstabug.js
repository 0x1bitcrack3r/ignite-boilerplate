const setupInstabug = async function(system, filesystem) {
  await system.spawn(`yarn add instabug-reactnative@2.8.1 --ignore-scripts`, {
    stdio: 'inherit'
  });

  // adding jest mock environments
  filesystem.copy(
    `${__dirname}/instabug-reactnative.js`,
    `${process.cwd()}/__mocks__/instabug-reactnative.js`,
    {
      overwrite: true
    }
  );
  // adding test case
  filesystem.copy(
    `${__dirname}/instabug.test.js`,
    `${process.cwd()}/__tests__/instabug.test.js`,
    {
      overwrite: true
    }
  );
};

module.exports = setupInstabug;
