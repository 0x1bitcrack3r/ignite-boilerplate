/**
 * @flow
 * @author: 0x1bitcrack3r
 */
import React, { Component } from 'react';
import { View, StatusBar, Platform } from 'react-native';
import { observer } from 'mobx-react';
import Instabug from 'instabug-reactnative';

// Others
import NavigationRouter from '../../navigation/NavigationRouter';

// Styles
import styles from './styles';

@observer
class RootContainer extends Component {
  componentWillMount() {
    if (Platform.OS === 'ios') {
      Instabug.startWithToken(
        'REDACTED',
        Instabug.invocationEvent.floatingButton
      );
    }
    Instabug.setAttachmentTypesEnabled(true, true, true, true, false);
  }
  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle="light-content" />
        <NavigationRouter />
      </View>
    );
  }
}

export default RootContainer;
