/**
 * @flow
 * @author: 0x1bitcrack3r
 **/

import Instabug from 'instabug-reactnative'

test('mocking Instabug', () => {
  Instabug.startWithToken(
    'startWithToken',
    Instabug.invocationEvent.floatingButton
  )
  Instabug.setAttachmentTypesEnabled(true, true, true, true, false)
  Instabug.invoke()
  Instabug.dismiss()
})
