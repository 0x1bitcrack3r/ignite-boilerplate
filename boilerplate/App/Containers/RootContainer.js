import React, { Component } from 'react';
import { View, StatusBar, Platform } from 'react-native';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import { connect } from 'react-redux';
import StartupActions from '../Redux/StartupRedux';
import { Sentry } from 'react-native-sentry';
// Styles
import styles from './Styles/RootContainerStyles';
import Instabug from 'instabug-reactnative';

Sentry.config(
  'REDACTED',
  {
    autoBreadcrumbs: {
      console: !__DEV__ // to get the file name and line numbers in dev mode debugging.
    }
  }
).install();
class RootContainer extends Component {
  componentDidMount() {
    this.props.startup();
  }
  componentWillMount() {
    if (Platform.OS === 'ios') {
      Instabug.startWithToken(
        'REDACTED',
        Instabug.invocationEvent.floatingButton
      );
    }
    Instabug.setAttachmentTypesEnabled(true, true, true, true, false);
  }

  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle="light-content" />
        <ReduxNavigation />
      </View>
    );
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
});

export default connect(
  null,
  mapDispatchToProps
)(RootContainer);
