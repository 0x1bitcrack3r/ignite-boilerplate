import React, { Component } from 'react';
import { View } from 'react-native';
import ExamplesRegistry from '../../App/Services/ExamplesRegistry';
import '../../ignite/Examples/Components/animatableExample';
// Styles
import styles from './Styles/RootContainerStyles';

class ExampleScreen extends Component {
	render() {
		return (
			<View style={styles.applicationView}>
				{ExamplesRegistry.renderPluginExamples()}
			</View>
		);
	}
}

export default ExampleScreen;
