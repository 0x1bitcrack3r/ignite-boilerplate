/**
 * The questions to ask during the install process.
 */
const questions = [
	{
		name: 'dev-screens',
		message: 'Would you like Ignite Development Screens?',
		type: 'list',
		choices: ['No', 'Yes']
	},
	// {
	// 	name: 'i18n',
	// 	message: 'What internationalization library will you use?',
	// 	type: 'list',
	// 	choices: ['none', 'react-native-i18n']
	// },
	{
		name: 'redux-persist',
		message: 'Would you like to include redux-persist?',
		type: 'list',
		choices: ['No', 'Yes']
	},
	{
		name: 'vector-icons',
		message: 'What vector icon library will you use?',
		type: 'list',
		choices: ['none', 'react-native-vector-icons']
	},
	{
		name: 'animatable',
		message: 'What animation library will you use?',
		type: 'list',
		choices: ['none', 'react-native-animatable']
	},
	{
		name: 'sentry',
		message: 'What sentry library will you use?',
		type: 'list',
		choices: ['none', 'react-native-sentry']
	},
	{
		name: 'instabug',
		message: 'What reporting tool will you use?',
		type: 'list',
		choices: ['none', 'instabug-reactnative']
	}
];

/**
 * The max preset.
 */
const max = {
	'dev-screens': 'Yes',
	//	i18n: 'react-native-i18n',
	'redux-persist': 'Yes',
	'vector-icons': 'react-native-vector-icons',
	animatable: 'react-native-animatable',
	sentry: 'react-native-sentry',
	instabug: 'instabug-reactnative'
};

/**
 * The min preset.
 */
const min = {
	'dev-screens': 'No',
	//	i18n: 'none',
	'redux-persist': 'No',
	'vector-icons': 'none',
	animatable: 'none',
	sentry: 'none',
	instabug: 'none'
};

module.exports = {
	questions,
	answers: { min, max }
};
